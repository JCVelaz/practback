var mongoose = require('mongoose');
const bcrypt=require('bcrypt');

const UsuarioSchema=new mongoose.Schema({
  nombre:{
    type:String,
    required:true
  },
  email:{
    type:String,
    unique:true,
    lowercase:true,
    required:true
  },
  password:{
    type:String,
    required:true
  }
},{timestamps:true});

UsuarioSchema.pre('save', function(next){
  const usuario=this;
  if(!usuario.isModified('password')){
    return next();
  }
  bcrypt.genSalt(10, function( err, salt) {
    if(err){
      next(err);
    }
    bcrypt.hash( usuario.password,  salt, function( err, hash) {
      if(err){
        next(err);
      }
      usuario.password=hash;
      next();
    });
  });
});

/*UsuarioSchema.methods.comparaPassword=function(contrasena, cb){
  bcrypt.compare(contrasena,this.contrasena, (err, sonIguales)=>{
    if(err){
      return cb(err);
    }
    cb(null, sonIguales);
  });
}*/

const Usuario=mongoose.model('Usuario',UsuarioSchema);

module.exports=Usuario;
