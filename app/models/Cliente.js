var mongoose = require('mongoose');

var movimientoSchema=new mongoose.Schema({
  importe:{
    type:Number,
    default:0
  },
  categoria:{
    type:String,
    default:null
  }
},{timestamps:true},{sparse:true} )

const ClienteSchema=new mongoose.Schema({
  numCuenta:{
    type:Number,
    unique:true,
    required:true
  },
  nombre:{
    type:String,
    required:true
  },
  apellido:{
    type:String,
    required:true
  },
  saldo:{
    type:Number,
    default:0
  },
  typoTarjeta:{
    type:String
  },
  /*listado:[
    {
      importe:{
        type:Number
      },
      categoria:{
        type:String
      }
    },{timestamps:true}
  ]*/
  listado:[movimientoSchema]
},{timestamps:true});

const Cliente=mongoose.model('Cliente',ClienteSchema);

module.exports=Cliente;
