var mongoose = require('mongoose');

const MovimientoSchema=new mongoose.Schema({
  nombre:{
    type:String,
    required:true
  },
  concepto:{
    type:String,
    required:true
  },
  importe:{
    type:Number
  },
  fecha: {
    type: Date,
    required: true,
    default: Date
  }
});

const Movimiento=mongoose.model('Movimiento',MovimientoSchema);

module.exports=Movimiento;
