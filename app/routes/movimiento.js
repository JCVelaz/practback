var express=require('express');
const MovimientosCtrl=require('../controllers/MovimientosController');

const Router=express.Router();

Router.get('/', MovimientosCtrl.index)//api.com/cliente  //Index: Lista todos los clientes
      .post('/', MovimientosCtrl.create)//api.com/cliente  //Crea un nuevo cliente
      .get('/:key/:value',MovimientosCtrl.busca,MovimientosCtrl.show)//api.com/cliente/id
      .put('/:key/:value',MovimientosCtrl.busca,MovimientosCtrl.update)//api.com/cliente/correo/jose@gmail.com
      .delete('/:key/:value',MovimientosCtrl.busca,MovimientosCtrl.remove);//api.com/cliente/correo/jose@gmail.com

module.exports=Router;
