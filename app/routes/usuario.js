var express=require('express');
const UsuarioCtrl=require('../controllers/UsuarioController');

const Router=express.Router();

Router.get('/', UsuarioCtrl.index)//api.com/cliente  //Index: Lista todos los clientes
      .post('/', UsuarioCtrl.create)//api.com/cliente  //Crea un nuevo cliente
      .post('/login', UsuarioCtrl.login)
      .get('/:key/:value',UsuarioCtrl.busca,UsuarioCtrl.show)//api.com/usuario/numCuenta/1234
      .put('/:key/:value',UsuarioCtrl.busca,UsuarioCtrl.update)//api.com/cliente/numCuenta/1234
      .delete('/:key/:value',UsuarioCtrl.busca,UsuarioCtrl.remove);//api.com/cliente/numCuenta/1234

module.exports=Router;
