var express=require('express');
const ClienteCtrl=require('../controllers/ClienteController');

const Router=express.Router();

Router.get('/', ClienteCtrl.index)//api.com/cliente  //Index: Lista todos los clientes
      .post('/', ClienteCtrl.create)//api.com/cliente  //Crea un nuevo cliente
      .post('/:key/:value/', ClienteCtrl.buscaMovimiento, ClienteCtrl.creaMovimiento)
      .get('/:key/:value',ClienteCtrl.busca,ClienteCtrl.show)//api.com/cliente/id
      //.put('/:key/:value',ClienteCtrl.busca,ClienteCtrl.update)//api.com/cliente/correo/jose@gmail.com
      .delete('/:key/:value',ClienteCtrl.busca,ClienteCtrl.remove);//api.com/cliente/correo/jose@gmail.com

module.exports=Router;
