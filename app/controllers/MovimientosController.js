const Movimiento = require('../models/Movimiento');

function index(req, res){
  Movimiento.find({})
      .then(movimientos =>{
        if(movimientos.length) return res.status(200).send({movimientos});
        return res.status(204).send({message:'No Contiene nada'});
      }).catch(err=>res.status(500).send({error}));
}

function show(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.movimientos)return res.status(404).send({message: 'No encontrado'});
  let movimientos=req.body.movimientos;
  return res.status(200).send({movimientos});
  //if(req.body.movimientos) return res.status(200).send({req.body.movimientos});
  //return res.status(404).send({message:'No encontrado'});
}

function create(req, res){
  let movimiento=new Movimiento(req.body);
  var registro = Date.now();
  req.body.fecha=registro;
  movimiento.save().then(movimiento => res.status(201).send({movimiento})).catch(error=>res.status(500).send({error}));
  //new Cliente(req.body)save().then(cliente => res.status(201).send({cliente})).catch(error=>res.status(500).send({error}));
}

function update(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.movimientos) return res.status(404).send({message:'No encontrado'});
  let movimiento=req.body.movimientos[0];
  movimiento=Object.assign(movimiento,req.body);
  movimiento.save().then(movimiento=>res.status(200).send({message:"Actualizado", movimiento})).catch(error=>res.status(500).send({error}));
}

function remove(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.movimientos) return res.status(404).send({message: 'No encontrado'});
  req.body.movimientos[0].remove().then(movimiento => res.status(200).send({message: 'Eliminado', movimiento})).catch(error=>res.status(500).send({error}));
}

function busca(req, res, next){
  let query={};
  query[req.params.key]=req.params.value
  Movimiento.find(query).then(movimientos=>{
    if(!movimientos.length) return next();
    req.body.movimientos=movimientos;
    return next();
  }).catch(error=>{
    req.body.error=error;
    next();
  })
}

module.exports={
  index,
  show,
  create,
  update,
  remove,
  busca
}
