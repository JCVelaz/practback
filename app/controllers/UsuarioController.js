const Usuario = require('../models/Usuario');
const bcrypt=require('bcrypt');

function index(req, res){
  Usuario.find({})
      .then(usuarios =>{
        if(usuarios.length) return res.status(200).send({usuarios});
        return res.status(204).send({message:'No Contiene nada'});
      }).catch(err=>res.status(500).send({error}));
}

function show(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.usuarios)return res.status(404).send({message: 'No encontrado'});
  let usuarios=req.body.usuarios;
  return res.status(200).send({usuarios});
}

function create(req, res){
  let usuario=new Usuario(req.body);
  Usuario.findOne({email:req.body.email},(err, usuarioExistente)=>{
    if(usuarioExistente){
      return res.status(400).send('Ya esta registrado este correo');
    }
    usuario.save().then(usuario => res.status(201).send({usuario})).catch(error=>res.status(500).send({error}));
    //usuario.save().then(req.logIn(usuario => res.status(201).send({usuario}))).catch(error=>res.status(500).send({error}));
  })
  //new Cliente(req.body)save().then(cliente => res.status(201).send({cliente})).catch(error=>res.status(500).send({error}));
}

function update(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.usuarios) return res.status(404).send({message:'No encontrado'});
  let usuario=req.body.usuarios[0];
  usuario=Object.assign(usuario,req.body);
  usuario.save().then(usuario=>res.status(200).send({message:"Actualizado", usuario})).catch(error=>res.status(500).send({error}));
}

function remove(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.usuarios) return res.status(404).send({message: 'No encontrado'});
  req.body.usuarios[0].remove().then(usuario => res.status(200).send({message: 'Eliminado', usuario})).catch(error=>res.status(500).send({error}));
}

function busca(req, res, next){
  let query={};
  query[req.params.key]=req.params.value
  Usuario.find(query).then(usuarios=>{
    if(!usuarios.length) return next();
    req.body.usuarios=usuarios;
    return next();
  }).catch(error=>{
    req.body.error=error;
    next();
  })
}

function login(req, res, next){
  Usuario.findOne({email:req.body.email},(err, usuarioExistente)=>{
    if(usuarioExistente){
      //console.log(usuarioExistente.password);
      bcrypt.compare(req.body.password,  usuarioExistente.password, function(error ,resA){
        if(resA){
          res.status(200).send('Usuario logado');
          return next();
        }
        res.status(400).send('email o password incorrectos');
      })
    }else{
      res.status(400).send('email o password incorrectos');
    }
  })
}

module.exports={
  index,
  show,
  create,
  update,
  remove,
  busca,
  login
}
