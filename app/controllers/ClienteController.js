const Cliente = require('../models/Cliente');

function index(req, res){
  Cliente.find({})
      .then(clientes =>{
        if(clientes.length) return res.status(200).send({clientes});
        return res.status(204).send({message:'No Contiene nada'});
      }).catch(err=>res.status(500).send({error}));
}

function show(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.clientes)return res.status(404).send({message: 'No encontrado'});
  let clientes=req.body.clientes;
  return res.status(200).send({clientes});
}

function create(req, res){
  Cliente.cleanIndexes();
  let cliente=new Cliente(req.body);
  cliente.save().then(cliente => res.status(201).send({cliente})).catch(error=>res.status(500).send({error}));
  /*let cliente=new Cliente({
    numCuenta:req.body.numCuenta,
    nombre:req.body.nombre,
    apellido:req.body.apellido,
	  saldo:req.body.saldo,
    typoTarjeta:req.body.typoTarjeta
  });
  cliente.save(
    function(err, contactoDB){
      if(err){
        return res.json({
          succes:false,
          msj:'El cliente no se registro',
          err
        });
      }else{
        return res.json({
          succes:true,
          msj:'El cliente se registro con exito',
        });
      }
    }
  );*/
  //new Cliente(req.body)save().then(cliente => res.status(201).send({cliente})).catch(error=>res.status(500).send({error}));
}

function creaMovimiento(req, res){
  //res.send('funcion crea listado');
  let resta=req.body.clientes[0].saldo-req.body.importe;
  //console.log(resta);
  Cliente.update({numCuenta:req.body.clientes[0].numCuenta},{
    $push:{
      "listado":{
        importe:req.body.importe,
        categoria:req.body.categoria
      }
    },
    $set:{
      saldo:resta
    }
  },function(error){
    if(error){
      return res.send(error);
    }else{
      return res.send('Listado registrado');
    }
  })
}

function update(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.clientes) return res.status(404).send({message:'No encontrado'});
  let cliente=req.body.clientes[0];
  cliente=Object.assign(cliente,req.body);
  cliente.save().then(cliente=>res.status(200).send({message:"Actualizado", cliente})).catch(error=>res.status(500).send({error}));
}

function remove(req, res){
  if(req.body.error) return res.status(500).send({error});
  if(!req.body.clientes) return res.status(404).send({message: 'No encontrado'});
  req.body.clientes[0].remove().then(cliente => res.status(200).send({message: 'Eliminado', cliente})).catch(error=>res.status(500).send({error}));
}

function busca(req, res, next){
  let query={};
  query[req.params.key]=req.params.value
  Cliente.find(query).then(clientes=>{
    if(!clientes.length) return next();
    req.body.clientes=clientes;
    return next();
  }).catch(error=>{
    req.body.error=error;
    next();
  })
}

function buscaMovimiento(req, res, next){
  let query={};
  query[req.params.key]=req.params.value
  Cliente.find(query).then(clientes=>{
    if(!clientes.length) return next();
    req.body.clientes=clientes;
    return next();
  }).catch(error=>{
    req.body.error=error;
    next();
  })
}

function buscaMovimiento(req, res, next){
  let query={};
  query[req.params.key]=req.params.value
  Cliente.find(query).then(clientes=>{
    if(!clientes.length) return next();
    req.body.clientes=clientes;
    return next();
  }).catch(error=>{
    req.body.error=error;
    next();
  })
}

module.exports={
  index,
  show,
  create,
  update,
  remove,
  busca,
  creaMovimiento,
  buscaMovimiento
}
