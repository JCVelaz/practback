var express=require('express');
var bodyParser=require('body-parser');

var app=express();
var Cliente=require('./routes/cliente');
var Usuario=require('./routes/usuario');
//var Movimiento=require('./routes/movimiento');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(function(req,res,next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.use('/clientes', Cliente);
app.use('/usuarios', Usuario);
//app.use('/movimientos', Movimiento);

module.exports=app;
