var Database=require('./app/config/database');
var CONFIG = require('./app/config/config');
var app= require('./app/app');

Database.connect();

app.listen(CONFIG.PORT,function(error){
  if(error) return console.log(error);
  console.log('Servidor corriendo en el puerto: '+CONFIG.PORT);
});
